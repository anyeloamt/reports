<?php namespace Artisans\Reports;

/**
 * Interface DataProviderInterface
 * @package Artisans\Reports
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 */
interface DataProviderInterface
{
    /**
     * Set the conditions to find data
     *
     * @param array $conditions
     */
    public function setConditions($conditions);

    /**
     * Gets the conditions to find data
     *
     * @return array
     */
    public function getConditions();

    /**
     * Sets report data
     *
     * @param array|object $data
     */
    public function setData($data);

    /**
     * Gets report data
     *
     * @return array|object
     */
    public function getData();

    /**
     * Sets the report
     *
     * @param \Artisans\Reports\ReportGeneratorInterface $report
     */
    public function setReport($report);

    /**
     * @return \Artisans\Reports\ReportGeneratorInterface
     */
    public function getReport();

    /**
     * Generates the data and config for a the report given.
     *
     * @param string $reportName
     * @return mixed
     * @throws \BadMethodCallException If report given doesn't exists.
     */
    public function generate($reportName);
} 