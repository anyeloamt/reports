<?php namespace Artisans\Reports;

/**
 * Class ExcelReport
 *
 * @package Artisans\Reports
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 *
 * TODO: Maybe some of the methods have to be moved to an interface.
 */
abstract class ExcelReportGenerator implements ReportGeneratorInterface
{

    /**
     * Data for report
     *
     * @var array|object
     */
    private $data;

    /**
     * Template file name.
     *
     * @var string
     */
    private $templateFileName;

    /**
     * Output file name.
     *
     * @var string
     */
    private $outputFileName;

    /**
     * Reports output directory.
     *
     * @var string
     */
    private $outputDir;

    /**
     * Reader type
     *
     * @var string
     */
    private $readerType = 'Excel5';

    /**
     * Writer type
     *
     * @var string
     */
    private $writerType = 'Excel5';

    /**
     * Default template directory.
     *
     * @var string
     */
    private $templateDir;

    /**
     * PHPExcel reference.
     *
     * @var \PHPExcel
     */
    private $excel;

    /**
     * Report data provider
     *
     * @var ReportDataProvider
     */
    private $dataProvider;

    /**
     * Set the report data.
     *
     * @param array|object $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Gets report data
     *
     * @return array|object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param \Artisans\Reports\DataProviderInterface $dataProvider
     */
    public function setDataProvider(DataProviderInterface $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * Returns the report data provider
     *
     * @return DataProviderInterface
     */
    public function getDataProvider()
    {
        return $this->dataProvider
            ?: $this->dataProvider = $this->buildDataProvider();
    }

    /**
     * @return DataProviderInterface
     */
    protected abstract function buildDataProvider();

    /**
     * @param \PHPExcel $excel
     */
    public function setExcel(\PHPExcel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * @return \PHPExcel
     */
    public function getExcel()
    {
        return $this->excel;
    }

    /**
     * @param string $outputFileName
     */
    public function setOutputFileName($outputFileName)
    {
        $this->outputFileName = $outputFileName;
    }

    /**
     * @param string $outputDir
     */
    public function setOutputDir($outputDir)
    {
        $this->outputDir = $outputDir;
    }

    /**
     * @return string
     */
    public function getOutputDir()
    {
        return $this->outputDir;
    }

    /**
     * @return string
     */
    public function getOutputFileName()
    {
        return $this->outputFileName;
    }

    /**
     * @param string $readerType
     */
    public function setReaderType($readerType)
    {
        $this->readerType = $readerType;
    }

    /**
     * @return string
     */
    public function getReaderType()
    {
        return $this->readerType;
    }

    /**
     * @param string $templateDir
     */
    public function setTemplateDir($templateDir)
    {
        $this->templateDir = $templateDir;
    }

    /**
     * @return string
     */
    public function getTemplateDir()
    {
        return $this->templateDir;
    }

    /**
     * @param string $templateFileName
     */
    public function setTemplateFileName($templateFileName)
    {
        $this->templateFileName = $templateFileName;
    }

    /**
     * @return string
     */
    public function getTemplateFileName()
    {
        return $this->templateFileName;
    }

    /**
     * @param string $writerType
     */
    public function setWriterType($writerType)
    {
        $this->writerType = $writerType;
    }

    /**
     * @return string
     */
    public function getWriterType()
    {
        return $this->writerType;
    }

    /**
     * Returns http headers
     *
     * @return array
     */
    protected function getHeaders()
    {
        return array(
            'Content-type' => 'application/x-msexcel',
            'Cache-control' => 'max-age=0',
        );
    }

    /**
     * Set conditions for the data provider.
     *
     * @param array|object $conditions
     */
    public function setConditions($conditions)
    {
        if ($this->getDataProvider()) {
            $this->getDataProvider()->setConditions($conditions);
        }
    }

    /**
     * Resets the report
     */
    public function reset()
    {
        $this->setTemplateFileName(null);
        $this->setOutputFileName(null);
        $this->setData(null);
    }

    /**
     * Begin the proccess to generate the report
     *
     * @param string $reportName Report name.
     */
    private function begin($reportName)
    {
        // If there is a template for the report load it.
        // Unless create a normal excel.

        if ( ! $this->getTemplateFileName() && ! $this->checkForTemplateFile($reportName)) {
            $this->setExcel(new \PHPExcel);
        } else {
            $reader = \PHPExcel_IOFactory::createReader($this->getReaderType());
            $this->setExcel($reader->load($this->getTemplateFileName()));
        }

        // Generate the data.
        if ($this->getDataProvider()) {
            if (method_exists($this->getDataProvider(), $reportName)) {
                $this->getDataProvider()->generate($reportName);
            }
        }
    }

    /**
     * Write the report to a file and save it.
     *
     * @param string $output
     */
    private function end($output = null)
    {
        $writer = \PHPExcel_IOFactory::createWriter($this->getExcel(), $this->getWriterType());

        if ( ! $output ) {
            $writer->save('php://output');
        } else {
            $writer->save($output);
        }
    }

    /**
     * @return string
     */
    protected function generateOutputFileName()
    {
        // TODO: Implement
    }

    /**
     * Generates the data and config for a the report given.
     *
     * @param string $reportName
     * @return mixed
     * @throws \BadMethodCallException If report given doesn't exists.
     */
    private function generate($reportName)
    {
        if ( ! method_exists($this, $reportName)) {

            throw new \BadMethodCallException("Report $reportName doesn't exist.");
        }
        return $this->$reportName();
    }

    /**
     * Downloads the report.
     *
     * @param string $reportName Report name
     */
    public function toBrowser($reportName)
    {
        $this->begin($reportName);
        $this->generate($reportName);

        foreach($this->getHeaders() as $key => $value) {
            header("$key:$value");
        }

        // TODO: Maybe we want to change the filename and extension.
        header('Content-disposition: attachment;filename="' . $reportName . '.xls"');

        $this->end();
    }

    /**
     * Saves the report to a file.
     *
     * @param string $reportName Report name
     * @param string|null $outputFileName Output file name.
     */
    public function toFile($reportName, $outputFileName = null)
    {
        $this->begin($reportName);

        if (empty($outputFileName)) {
            $outputFileName = $this->getOutputFileName() ?: $this->generateOutputFileName();
        }

        $this->generate($reportName);
        $this->end($outputFileName);
    }

    /**
     * Check if a template file exists.
     *
     * @param string $reportName
     *
     * @return bool True if file exists
     */
    private function checkForTemplateFile($reportName)
    {
        $fileName = $this->getTemplateDir()
            . DIRECTORY_SEPARATOR
            . $reportName
            . $this->getExcelExtension();

        $this->setTemplateFileName($fileName);

        return file_exists($this->getTemplateFileName());
    }

    /**
     * Gets excel extension
     *
     * @return string Excel extension
     */
    protected function getExcelExtension()
    {
        return ".xlsx";
    }
}