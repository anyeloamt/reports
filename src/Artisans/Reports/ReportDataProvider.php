<?php namespace Artisans\Reports;

/**
 * Class ReportDataProvider
 * @package Artisans\Reports
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 */
abstract class ReportDataProvider implements DataProviderInterface
{

    /**
     * Report data
     *
     * @var array|object
     */
    protected $data;

    /**
     * Report
     *
     * @var ExcelReportGenerator
     */
    protected $report;

    /**
     * Report conditions
     *
     * @var array
     */
    protected $conditions;

    /**
     * @param ExcelReportGenerator $report
     */
    public function __construct(ExcelReportGenerator $report)
    {
        $this->setReport($report);
        $this->getReport()->setDataProvider($this);
    }


    /**
     * @param array $conditions
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * @return array
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param array|object $data
     */
    public function setData($data)
    {
        $this->data = $data;

        if ($this->getReport()) {
            $this->getReport()->setData($data);
        }
    }

    /**
     * @return array|object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param \Artisans\Reports\ExcelReportGenerator $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return \Artisans\Reports\ExcelReportGenerator
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Generates the data and config for a the report given.
     *
     * @param string $reportName
     * @return mixed
     * @throws \BadMethodCallException If report given doesn't exists.
     */
    public function generate($reportName)
    {
        if ( ! method_exists($this, $reportName)) {

            throw new \BadMethodCallException("Report $reportName doesn't exist.");
        }
        return $this->$reportName();
    }
} 