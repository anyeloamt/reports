<?php namespace Artisans\Reports;

/**
 * Interface ReportGeneratorInterface
 * @package Artisans\Reports
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 */
interface ReportGeneratorInterface
{
    /**
     * Returns the report data provider
     *
     * @return DataProviderInterface
     */
    public function getDataProvider();

    /**
     * Gets report data
     *
     * @return array|object
     */
    public function getData();

    /**
     * Set the report data.
     *
     * @param array|object $data
     */
    public function setData($data);

    /**
     * Set conditions for the data provider.
     *
     * @param array|object $conditions
     */
    public function setConditions($conditions);
} 